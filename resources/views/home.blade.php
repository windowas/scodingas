@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div>
                        @if(Session::has('success'))
                            {{Session::get('success')}}
                        @endif
                    </div>

                    <div class="row">

                        <a href="{{route('ads.create')}}" class="btn btn-primary m-1">Create</a>
                        <a href="{{route('ads.index')}}" class="btn btn-primary m-1 float-right">Return</a>
                        <table class="table sortable">
                            <thead>
                            <tr>
                                <th>@sortablelink('created_at', 'Post date')</th>
                                <th>@sortablelink('title', 'Title')</th>
                                <th>@sortablelink('description', 'Description')</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($ads as $ad)
                                <tr>
                                    <td>{{$ad->created_at}}</td>
                                    <td>{{$ad->title}}</td>
                                    <td>{{$ad->description}}</td>
                                    <td>
                                        <form action="{{route('ads.destroy', $ad)}}" method="POST">
                                            <a href="ads/{{$ad->id}}/edit" class="btn btn-warning btn-small">Edit</a>
                                            <button class="btn btn-danger btn-small" type="submit">Delete</button>
                                            <input type="hidden" name="id" value="{{$ad->id}}">
                                            @csrf
                                            @method("DELETE")
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
