@extends('layouts.app')

@section('content')

    <div class="container">
        <h1>Welcome to Ads page</h1>

        <div>
            @if(Session::has('success'))
                {{Session::get('success')}}
            @endif
        </div>

        <div class="row">
            @if (Auth::user())
            <a href="{{route('ads.create')}}" class="btn btn-primary m-1 right">Create</a>
            @endif
            <table class="table">
                <thead>
                    <tr>
                        <th>@sortablelink('created_at', 'Post date')</th>
                        <th>@sortablelink('title', 'Title')</th>
                        <th>@sortablelink('description', 'Description')</th>
                        <th>@sortablelink('user', 'User')</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($ads as $ad)
                        <tr>
                            <td>{{$ad->created_at}}</td>
                            <td>{{$ad->title}}</td>
                            <td>{{$ad->description}}</td>
                            <td>{{$users->find($ad->user)->name}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection