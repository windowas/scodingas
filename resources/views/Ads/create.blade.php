@extends('layouts.app')

@section('content')

    <div class="container">
        <h1>Create an Ad</h1>

        <a href="{{route('ads.index')}}" class="btn btn-primary float-right">Return</a>

        <div class="row">
            <div class="col-md-6 col-offset-3">

                @if($errors->all())
                    <div class="panel">
                        <ul class="alert-danger rounded">
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form action="{{route('ads.store')}}" method="POST">
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" name="title">
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea class="form-control" name="description"></textarea>
                    </div>

                    <button type="submit" class="btn btn-success btn-block">Submit</button>
                    @csrf
                </form>
            </div>
        </div>
    </div>
@endsection