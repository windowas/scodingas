<?php

namespace App\Http\Controllers;

use App\Ads;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\adsValidate;
use Illuminate\Support\Facades\Auth;

class AdsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $ads = Ads::sortable(['created_at' => 'desc'])->paginate(10);
        $users = User::all();
        return view('Ads.index', compact('ads', 'users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Ads.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(adsValidate $validate)
    {

        $parameters = $validate->all();
        $parameters['user'] = Auth::user()->id;

        Ads::create($parameters);

        return redirect('/ads');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ads  $ads
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ad = Ads::find($id);
        if ($ad && Auth::user()->id == $ad->user) {
            return view('Ads.edit', compact('ad'));
        } else {
            return redirect('/ads');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ads  $ads
     * @return \Illuminate\Http\Response
     */
    public function update($id, adsValidate $validate)
    {

        Ads::whereId($id)->update($validate->except(['_method','_token']));

        return redirect('/ads');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ads  $ads
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $ads)
    {
        $ads = Ads::find($ads->id);
        $ads->delete();
        return redirect('/ads')->with('success', 'You have deleted a post.');
    }
}
