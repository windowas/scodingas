<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Ads extends Model
{
    use Sortable;

    protected $fillable = ['title', 'description', 'user'];

    public $sortable = [
        'created_at',
        'user',
        'title',
        ];

    public function user(){
        return $this->belongsTo('App\User', 'user', 'id');
    }
}
